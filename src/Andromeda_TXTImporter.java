import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.jabref.BibtexEntry;
import net.sf.jabref.BibtexEntryType;
import net.sf.jabref.OutputPrinter;
import net.sf.jabref.imports.ImportFormat;
import java.nio.charset.StandardCharsets;

public class Andromeda_TXTImporter extends ImportFormat {
	public static List<String> T_BOOK = Collections.unmodifiableList(Arrays.asList(new String[] { "KNJIGA", "KATALOG ZBIRKE" }));
	public static List<String> T_ARTICLE = Collections.unmodifiableList(Arrays.asList(new String[] { "ČLANAK" }));
	public static List<String> T_INBOOK = Collections
			.unmodifiableList(Arrays.asList(new String[] { "POGLAVLJE U KNJIZI", "POGLAVLJE U KATALOGU ZBIRKE", "ČLANAK U ZBORNIKU" }));
	public static List<String> T_MASTERSTHESIS = Collections
			.unmodifiableList(Arrays.asList(new String[] { "DISERTACIJA (diplomski rad, magisterij, doktorat)" }));
	public static List<String> T_TECHREPORT = Collections.unmodifiableList(Arrays.asList(new String[] { "IZVJEŠTAJ" }));
	public static List<String> T_BOOKLET = Collections.unmodifiableList(Arrays.asList(new String[] { "BROŠURA", "KNJIŽICA", "BROŠURA/KNJIŽICA" }));
	public static String newLine = System.getProperty("line.separator");
	public String path = "";

	public static String ENCODING_UTF16 = "UTF-16";
	public static String ENCODING_UTF8 = "UTF-8";
	public static String ENCODING_CP1250 = "Cp1250";
	public static String ENCODING_CP1252 = "Cp1252";
	public static String ENCODING_88592 = "ISO-8859-2";
	public static String ENCODING_UTF16BE = "UTF-16BE";
	public static String ENCODING_UTF16LE = "UTF-16LE";

	Set<String> encodings = new HashSet<String>(Arrays.asList(ENCODING_UTF16, ENCODING_UTF8, ENCODING_CP1250, ENCODING_88592, ENCODING_UTF16LE));

	// String delimiter = ";";
	String delimiter = "\t";

	@Override
	public String getFormatName() {
		return "Andromeda Tab Separated Importer";
	}

	@Override
	public String getExtensions() {
		return "*.txt";
	}

	@Override
	public boolean isRecognizedFormat(InputStream stream) throws IOException {
		return isRecognizedFormat(stream, ENCODING_UTF8);
	}

	public boolean isRecognizedFormat(InputStream stream, String encoding) throws IOException {
		logLineToFile(newLine + newLine + "**************************************");
		logLineToFile(  "###########   " + (new Date()) + "    #############");
    	logLineToFile( "**************************************");
    	logLineToFile("Testing recognizable formats for file with encoding: " + encoding);

   		boolean check = checkEncodingFileFormat(stream, encoding);
   		if(check == true){
   			String message = "- Successful file open for  " + encoding + " encoding";
    		logLineToFile(message);
		}else{
			String message = "- Unsuccessful reading for " + encoding + " encoding";
      		logLineToFile(message);    				
		}
    	return check;
    }

	public boolean checkEncodingFileFormat(InputStream stream, String encoding) throws IOException {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(stream, encoding));
			String lajn = "";

			String[] data;
			int rowNum = 0;
			data = lajn.split(delimiter);
			while ((lajn = in.readLine()) != null) {
				data = lajn.split(delimiter);
				rowNum++;
				if (!checkForNumberOfColumns(data)) {
					String message = "Error wrong number of columns in row " + rowNum+ "! Found " + data.length + " expected 21 (or 20) in row " + rowNum;
					logLineToFile(message);
					return false;
				}
			}
		} catch (RuntimeException ex) {
			logLineToFile("Error while opening file! Unrecognized format:" + ex.getMessage());
			throw new IOException(ex.getMessage());
		}
		return true; // this is discouraged except for demonstration purposes

	}

	private boolean checkForNumberOfColumns(String[] data) {
		boolean flag = false;
		if (data.length != 21 && data.length != 20) {
			flag = false;
			logLineToFile("Wrong number of columns; Found " + data.length + " columns");
		} else {
			flag = true;
		}
		return flag;
	}

	@Override
	public List<BibtexEntry> importEntries(InputStream stream, OutputPrinter status) throws IOException {
		List<BibtexEntry> importEntriesWithEncoding = importEntriesWithEncoding(stream, status, ENCODING_UTF8);
		return importEntriesWithEncoding;
	}

	protected List<BibtexEntry> importEntriesWithEncoding(InputStream stream, OutputPrinter status, String encoding)
			throws UnsupportedEncodingException, IOException {
		logLineToFile(newLine + newLine + "**************************************");
		logLineToFile(newLine + "******* Importing entries with encoding: " + encoding + "******* ");
		status.setStatus("Importing entries " + new Date());

		BufferedReader in = new BufferedReader(new InputStreamReader(stream, encoding));
		String line = null;
		int lineCount = 0;
		int importCount = 0;

		List<BibtexEntry> bibitems = new ArrayList<>();

		BibtexEntry entry = null;
		while ((line = in.readLine()) != null) {
			lineCount++;
			if (lineCount == 1) {
				// header row
				continue;
			}
			status.setStatus("loading: " + line);
			logToFile(" . ");
			try {
				entry = parseLine(line);
			} catch (RuntimeException e) {
				logToFile("ERROR:" + line + newLine + e.getMessage());
			}

			if (entry != null) {
				bibitems.add(entry);
				importCount++;
			} else {
				// Gore je logirano
				//logToFile("\n Error in line " + line + "\n");
			}
		}
		logLineToFile("\n");
		String msg = "Imported " + importCount + " entries from  " + lineCount + " lines.";
		logLineToFile(msg);
		status.setStatus(msg);
		status.showMessage(msg);
		in.close();
		return bibitems;
	}

	private BibtexEntry parseLine(String line) {
		String[] data = line.split(delimiter);
		// int authorCount = data.length - 20;
		// String comma = "";
		Record rec = this.new Record();
		int i = 0;
		int ai = 0;

		rec.id = data[i++];
		rec.referenceType = data[i++];
		rec.author = trimDoubleQuotesAndReplaceDoublesForAuthors(data[i++]);

		/*
		 * Ukoliko bude ; a ima je u autoru a radi se sa ; delimiterom
		 * rec.author = ""; String tmp = ""; while(ai<authorCount){ tmp =
		 * data[i++]; //Ako je godina, znaci da ima vise urednika a ne autora
		 * if(isNumeric(tmp)){ i--; ai--; break; } rec.author += comma + tmp;
		 * comma = "; "; ai++; }
		 */
		rec.year = data[i++];
		rec.Title = trimDoubleQuotesAndReplaceDoubles(data[i++]);
		rec.TitleLong = trimDoubleQuotesAndReplaceDoubles(data[i++]);
		rec.journal = data[i++];
		rec.volume = data[i++];
		rec.number = data[i++];
		rec.pages = data[i++];
		rec.pageCount = data[i++];
		rec.editor = trimDoubleQuotesAndReplaceDoublesForAuthors(data[i++]);
		/*
		 * 
		 * rec.editor = ""; tmp = ""; comma = ""; while(ai<authorCount){ tmp =
		 * data[i++]; rec.editor += comma + tmp; comma = "; "; ai++; }
		 */

		rec.publisher = data[i++];
		rec.publisherLocation = data[i++];
		rec.web = data[i++];
		rec.visitDate = data[i++];
		rec.referenceMark = data[i++];
		rec.note = data[i++];
		rec.enteredBy = data[i++];
		rec.contractCategory = data[i++];
		if (data.length > 20) {
			rec.dateEntry = data[i++];
		}

		logLineToFile(rec.toString());
		BibtexEntry entry = entryFromRecord(rec);
		return entry;
	}

	private BibtexEntry entryFromRecord(Record record) {

		BibtexEntry entry = null;

		if (T_BOOK.contains(record.referenceType)) {
			entry = createBookEntry(record);
		} else if (T_ARTICLE.contains(record.referenceType)) {
			entry = createArticleEntry(record);
		} else if (T_INBOOK.contains(record.referenceType)) {
			entry = createInBookEntry(record);
		} else if (T_MASTERSTHESIS.contains(record.referenceType)) {
			entry = createMasterthesisEntry(record);
		} else if (T_TECHREPORT.contains(record.referenceType)) {
			entry = createTechreportEntry(record);
		} else if (T_BOOKLET.contains(record.referenceType)) {
			entry = createBookletEntry(record);
		} else {
			logLineToFile("Unrecognized reference type: " + record);
		}

		return entry;
	}

	private BibtexEntry createBookletEntry(Record record) {
		BibtexEntry entry = new BibtexEntry("1", BibtexEntryType.BOOKLET);
		entry.setField("author", record.author);
		entry.setField("title", record.Title);
		entry.setField("year", record.year);
		entry.setField("howpublished", record.publisher);

		entry.setField("address", record.publisherLocation);
		entry.setField("note", record.note);

		return entry;
	}

	private BibtexEntry createMasterthesisEntry(Record record) {
		BibtexEntry entry = new BibtexEntry("1", BibtexEntryType.MASTERSTHESIS);
		entry.setField("author", record.author);
		entry.setField("title", record.Title);
		entry.setField("year", record.year);
		entry.setField("institution", record.publisher);

		entry.setField("address", record.publisherLocation);
		entry.setField("note", record.note);

		return entry;
	}

	private BibtexEntry createTechreportEntry(Record record) {
		BibtexEntry entry = new BibtexEntry("1", BibtexEntryType.TECHREPORT);
		entry.setField("author", record.author);
		entry.setField("title", record.Title);
		entry.setField("year", record.year);
		entry.setField("institution", record.publisher);

		entry.setField("address", record.publisherLocation);
		entry.setField("note", record.note);

		return entry;
	}

	private BibtexEntry createInBookEntry(Record record) {
		BibtexEntry entry = new BibtexEntry("1", BibtexEntryType.INBOOK);
		entry.setField("title", record.Title);
		entry.setField("booktitle", record.TitleLong);
		entry.setField("publisher", record.publisher);
		entry.setField("year", record.year);
		entry.setField("author", record.author);
		entry.setField("editor", record.editor);
		entry.setField("address", record.publisherLocation);
		entry.setField("pages", record.pages);

		entry.setField("volume", record.volume);
		entry.setField("edition", record.number);
		entry.setField("url", record.web);
		entry.setField("note", record.note);

		return entry;
	}

	private BibtexEntry createArticleEntry(Record record) {
		BibtexEntryType article = BibtexEntryType.ARTICLE;
		BibtexEntry entry = new BibtexEntry("1", article);
		entry.setField("author", record.author);
		entry.setField("title", record.Title);
		entry.setField("journal", record.journal);
		entry.setField("year", record.year);
		entry.setField("volume", record.volume);
		entry.setField("pages", record.pages);

		entry.setField("edition", record.number);
		entry.setField("address", record.publisherLocation);
		entry.setField("publisher", record.publisher);
		entry.setField("editor", record.editor);
		entry.setField("pages", record.pages);
		entry.setField("url", record.web);
		entry.setField("note", record.note);

		return entry;
	}

	private BibtexEntry createBookEntry(Record record) {
		BibtexEntry entry = new BibtexEntry("1", BibtexEntryType.BOOK);
		entry.setField("title", record.Title);
		entry.setField("publisher", record.publisher);
		entry.setField("year", record.year);
		entry.setField("author", record.author);
		entry.setField("address", record.publisherLocation);
		entry.setField("editor", record.editor);

		entry.setField("volume", record.volume);
		entry.setField("pages", record.pages);
		entry.setField("edition", record.number);
		entry.setField("url", record.web);
		entry.setField("note", record.note);

		return entry;
	}

	private class Record {
		String id;
		String referenceType;
		String author;
		String year;
		String Title;
		String TitleLong;
		String journal;
		String volume;
		String number;
		String pages;
		String pageCount;
		String editor;
		String publisher;
		String publisherLocation;
		String web;
		String visitDate;
		String referenceMark;
		String note;
		String enteredBy;
		String contractCategory;
		String dateEntry;

		@Override
		public String toString() {
			return "Record [id=" + id + ", referenceType=" + referenceType + ", author=" + author + ", year=" + year + ", Title=" + Title + ", TitleLong="
					+ TitleLong + ", journal=" + journal + ", volume=" + volume + ", number=" + number + ", pages=" + pages + ", pageCount=" + pageCount
					+ ", editor=" + editor + ", publisher=" + publisher + ", publisherLocation=" + publisherLocation + ", web=" + web + ", visitDate="
					+ visitDate + ", referenceMark=" + referenceMark + ", note=" + note + ", enteredBy=" + enteredBy + ", contractCategory=" + contractCategory
					+ ", dateEntry=" + dateEntry + "]";
		}
	}

	private boolean isNumeric(String s) {
		return s.matches("[-+]?\\d*\\.?\\d+");
	}

	private void logLineToFile(String logEntry) {
		logToFile(logEntry + newLine);
	}

	private void logToFile(String logEntry) {
		FileWriter fw = null;
		BufferedWriter bw = null;

		String FILENAME = "andromeda.log";

		try {
			File file = new File(FILENAME);

			if (!file.exists()) {
				file.createNewFile();
			}

			fw = new FileWriter(file.getAbsoluteFile(), true);
			bw = new BufferedWriter(fw);

			bw.write(logEntry);

		} catch (IOException e) {
			// throw new RuntimeException("Error while logging");
			// ako je ovo odkomentirano, rikne import
		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}
	}

	public static String trimDoubleQuotesAndReplaceDoublesForAuthors(String text) {
		text = text.replaceAll(";", " and");
		text = trimDoubleQuotesAndReplaceDoubles(text);
		return text;
	}

	public static String trimDoubleQuotesAndReplaceDoubles(String text) {
		int textLength = text.length();

		if (textLength >= 2 && text.charAt(0) == '"' && text.charAt(textLength - 1) == '"') {
			text = text.substring(1, textLength - 1);
		}
		text = text.replaceAll("\"\"", "\"");
		return text;
	}

	public String getDescription() {
		return "Andromeda importer for tab separated files";
	}
}
