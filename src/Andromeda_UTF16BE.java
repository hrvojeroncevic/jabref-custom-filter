import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import net.sf.jabref.BibtexEntry;
import net.sf.jabref.OutputPrinter;

public class Andromeda_UTF16BE extends Andromeda_TXTImporter {
	private static final String ENCODING_UTF16BE = null;
	String DEFAULT_ENCODING = ENCODING_UTF16BE;

	@Override
	public boolean isRecognizedFormat(InputStream stream) throws IOException {
		return isRecognizedFormat(stream, DEFAULT_ENCODING);
	}

	@Override
	public List<BibtexEntry> importEntries(InputStream stream, OutputPrinter status) throws IOException {
		List<BibtexEntry> importEntriesWithEncoding = importEntriesWithEncoding(stream, status, DEFAULT_ENCODING);
		return importEntriesWithEncoding;
	}

	
}
