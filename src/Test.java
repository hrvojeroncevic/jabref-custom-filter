import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Path;

import net.sf.jabref.OutputPrinter;

public class Test {
	
	public static void main(String[] args) {
		
		String displayName = Charset.defaultCharset().displayName();
		
		//Path path = FileSystems.getDefault().getPath("D:/myProjects/Jana/", "Obrazac_za_upis_literature_Template.csv");
		Path path = FileSystems.getDefault().getPath("D:/myProjects/Jana/", "JanaTestiranje20170319/Test3.txt");
//		Path path = FileSystems.getDefault().getPath("D:/myProjects/Jana/", "Test-allTypes.txt");
		try {
			
			FileInputStream file = new FileInputStream(path.toString());
			Andromeda_TXTImporter imp = new Andromeda_TXTImporter();
			OutputPrinter op = new OutputPrinter() {
				
				@Override
				public void showMessage(Object message, String title, int msgType) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void showMessage(String string) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void setStatus(String s) {
					// TODO Auto-generated method stub
					
				}
			};
			imp.isRecognizedFormat(file);
			imp.importEntries(file, op);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
